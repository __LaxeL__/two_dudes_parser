import csv
import getpass
import os
import pickle
import threading
import time
import traceback

import pymongo
import vk_api
import vk_api.execute

import config


class Parser:
    """
    :param bots_vk_sessions: Содержит сессии vk, через которые будет происходить парсинг
        Чем больше сессий, тем больше потоков => быстрее парсинг
    :type bots_vk_sessions: list[vk_api.VkApi]

    :param bots_threads: Содержит потоки для каждой сессии
    :type bots_threads: list[threading.Thread]

    :param client: Содержит клиент mongo db
    :type client: pymongo.MongoClient

    :param db: Сеанс БД
    :type db: pymongo.database.Database

    :param __groups_file: Csv файл из которого создается очередь из групп для парсинга
    :type __groups_file: _io.TextIOWrapper

    :param counter: Счетчик пройденных групп, постов, количество ошибок
    :type counter: dict

    :param groups_generator: Генератор групп, поддерживает next()
    :type groups_generator: iterator

    :param groups_generator_locker: Блокировка для предотвращения обращений к groups_generator
        в то время, когда генератор обрабатывается другим потоком
    :type groups_generator_locker: threading.Lock

    """
    def __init__(self):
        self.create_necessary_files()
        self.bots_vk_sessions: list = self.create_bots()
        self.bots_threads: list = []
        self.client, self.db = self.connect_to_database()
        self.__groups_file = open(config.path_to_groups_list_csv_default)
        self.counter = {
            'groups': 0,
            'posts': 0,
            'errors': 0
        }
        self.groups_generator = self.get_groups_generator()
        self.groups_generator_locker = threading.Lock()

    def create_necessary_files(self):
        if not os.path.exists(config.path_to_vk_accounts):
            print('Нет аккаунтов для парсинга')
            accounts = []
            while True:
                login = input('Введите логин: ')
                password = getpass.getpass('Введите пароль: ')
                accounts.append(f'{login} {password}')
                if input('Добавить еще аккаунт?(y/n): ').lower() != 'y':
                    break
            with open(config.path_to_vk_accounts, 'w') as f:
                f.write('\n'.join(accounts))

    def create_bots(self):
        accounts = []
        with open(config.path_to_vk_accounts) as f:
            data = [line.strip().split() for line in f]
        for login, password in data:
            try:
                vk = vk_api.VkApi(login, password, auth_handler=self.auth_handler,
                                  captcha_handler=self.captcha_handler)
                vk.auth()
                accounts.append(vk)
            except vk_api.exceptions.BadPassword:
                print(f'Неверный логин({login}) или пароль')
            except Exception:
                print(traceback.format_exc())
        if accounts:
            return accounts
        else:
            raise NoValidAccounts

    def auth_handler(self):
        """
        При двухфакторной аутентификации вызывается эта функция.
        """
        key = input("Код двухфакторной авторизации: ")
        remember_device = True
        return key, remember_device

    def captcha_handler(captcha):
        """
        При возникновении капчи вызывается эта функция и ей передается объект
        капчи. Через метод get_url можно получить ссылку на изображение.
        Через метод try_again можно попытаться отправить запрос с кодом капчи
        """

        key = input("Введите капчу {0}: ".format(captcha.get_url())).strip()

        # Пробуем снова отправить запрос с капчей
        return captcha.try_again(key)

    # TODO
    def get_groups_generator(self):
        reader = csv.reader(self.__groups_file)
        if os.path.exists('groups_file_status.pickle'):
            with open('groups_file_status.pickle', 'rb') as f:
                file = pickle.load(f)
                for _ in range(file['groups']):
                    next(reader)
                self.counter = file
        return reader

    def connect_to_database(self):
        print('Подключение к серверу')
        client = pymongo.MongoClient(
            *config.mongo_db_config['connect'], serverSelectionTimeoutMS=5000
        )
        try:
            client.admin.command('ismaster')
        except pymongo.errors.ConnectionFailure:
            print('Не удалось подключиться')
            raise ConnectionToDatabaseError
        except Exception:
            print(traceback.format_exc())
        else:
            print('Успешно')
        if config.mongo_db_config['database_name'] not in client.list_database_names():
            print('Базы данных нет, создана')
        db = client[config.mongo_db_config['database_name']]
        if config.mongo_db_config['posts_collection_name'] not in db.list_collection_names():
            db.create_collection(config.mongo_db_config['posts_collection_name'])
            print('Коллекции постов нет, создана')
        path_to_groups_list_csv = config.path_to_groups_list_csv_default
        if input(f'Записать {path_to_groups_list_csv} в базу данных(y/n)?: ').lower() == 'y':
            self.upload_groups_csv_to_db(db)
        return client, db

    def upload_groups_csv_to_db(self, db):
        path_to_groups_list_csv = config.path_to_groups_list_csv_default
        groups_collection_name = config.mongo_db_config['groups_collection_name']
        if os.path.exists(path_to_groups_list_csv):
            f = open(path_to_groups_list_csv)
            reader = csv.reader(f)
            try:
                if groups_collection_name not in db.list_collection_names():
                    db.create_collection(groups_collection_name)
                    print('Коллекции групп нет, создана')
                else:
                    db.drop_collection(groups_collection_name)
                    print('Колекция групп перезаписана')
                    db.create_collection(groups_collection_name)
                upload_counter = 0
                for group_id, group_name in reader:
                    db[groups_collection_name].insert_one({
                        'group_id': group_id,
                        'group_name': group_name
                    })
                    upload_counter += 1
                    print(f'Выгружено {upload_counter} строк\r', end='')
                print()
            except KeyboardInterrupt:
                print()
                print('Выгрузка прервана')
            except Exception:
                print(traceback.format_exc())
        else:
            print('Файл не найден, выгрузка произведена не будет')

    def run(self):
        self.bots_threads.clear()
        for bot_vk_session in self.bots_vk_sessions:
            thread = Bot(
                bot_vk_session, self.groups_generator, self.groups_generator_locker,
                self.db, self.counter
            )
            thread.daemon = True
            thread.start()
            self.bots_threads.append(thread)
        for bot_thread in self.bots_threads:
            bot_thread.join()


class Bot(threading.Thread):
    def __init__(self, vk_session, groups_generator, groups_generator_locker, db, counter):
        super().__init__()
        self.__vk_session: vk_api.VkApi = vk_session
        self.fullname: str = f'{self.name}-{self.__vk_session.login}'
        self.groups_generator: iterator = groups_generator
        self.groups_generator_locker: threading.Lock = groups_generator_locker
        self.db = db
        self.vk_wall_get_function = vk_api.execute.VkFunction(
            args=('ids',),
            return_raw='execute_errors',
            code='''
                var res = [];
                var ids = %(ids)s;
                    var i = 0;

                    while (i < ids.length) {
                        res.push(
                            API.wall.get({
                                    "owner_id": ids[i],
                                    "count": 1
                                })
                        );
                        i = i + 1;
                    }
                    return res;
        ''')
        self.counter = counter

    def run(self):
        # TODO: Счетчики приделать
        while True:
            try:
                self.groups_generator_locker.acquire()
                groups, do_next = self.get_next_groups()
                self.groups_generator_locker.release()
                posts = self.get_latest_posts_at_groups(groups)
                for post in posts:
                    self.upload_post_to_db(post)
                    self.counter['posts'] += 1
                if not do_next:
                    raise StopIteration
            except StopIteration:
                print(self.fullname + 'группы кончились')
                break
            except Exception:
                print(traceback.format_exc())
                self.counter['errors'] += 1
            print(
                'Счетчик:', self.counter["groups"], 'групп',
                self.counter["posts"], 'постов',
                self.counter["errors"], 'ошибок\r',
                end=''
            )
        print()

    def get_next_groups(self):
        ids = []
        for _ in range(25):
            try:
                ids.append(next(self.groups_generator))
            except StopIteration:
                return ids, False
            finally:
                with open('groups_file_status.pickle', 'wb') as f:
                    pickle.dump(self.counter, f)
        return ids, True

    def get_latest_posts_at_groups(self, groups):
        group_ids = list(map(lambda x: -int(x[0]), groups))
        response = self.vk_wall_get_function(self.__vk_session, group_ids)
        if 'execute_errors' in response and response['execute_errors'][0]['error_code'] == 29:
            break_time_unix = time.time() + 10800
            break_time_str = time.strftime('%X %d/%m/%Y', time.localtime(break_time_unix))
            print(f'{self.fullname} - Приостановлено до {break_time_str} по причине лимита')
            time.sleep(10800)
            self.get_latest_posts_at_groups(groups)
        self.counter['groups'] += len(groups)
        result = []
        for post in response:
            if isinstance(post, dict) and post.get('items'):
                result.append(post['items'][0])
        return result

    def upload_post_to_db(self, post):
        collection_name = config.mongo_db_config['groups_collection_name']
        data = {
            'post': post
        }
        self.db[collection_name].insert_one(data)


# EXCEPTIONS

class NoValidAccounts(Exception):
    pass

class ConnectionToDatabaseError(Exception):
    pass
