# Кэширование данных аккаунтов vk
path_to_vk_config = 'vk_config.json'
# Путь к текстовому файлу, в котором хранятся логины и пароли от вк
path_to_vk_accounts = 'accounts.txt'
# Путь к csv файлу, в котором id и названия ~57 млн групп
path_to_groups_list_csv_default = '../two_dudes/groups.csv'

# Конфиг для покдлючения к БД
mongo_db_config = {
    'connect': ('localhost', 27017),
    'database_name': 'two_dudes_parser',
    'posts_collection_name': 'posts',
    'groups_collection_name': 'groups'
}