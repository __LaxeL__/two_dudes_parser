import engine
import traceback


def main():
    parser = engine.Parser()
    try:
        parser.run()
    except KeyboardInterrupt:
        parser.client.close()
        print('\nКлиент закрыт')
    except Exception:
        print(traceback.format_exc())


if __name__ == '__main__':
    main()
